import {combineReducers} from "redux";
import {createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

import {repos} from "../reducers/repos";

const reducer = combineReducers({
	repos,
})

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))