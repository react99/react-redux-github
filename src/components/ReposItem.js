import './reposItem.scss'
import React from "react";
import {Link} from "react-router-dom";

export const ReposItem = ({repo}) => {
	return (
		<div className="repo">
			<div className="repo__header">
				<div className="repo__header-name"><Link to={`/card/${repo.owner.login}/${repo.name}`}>Название репозитория: {repo.name}</Link></div>
				<div className="repo__header-stars">Количество звезд: {repo.stargazers_count}</div>
			</div>
			<div className="repo__last-commit">Последний коммит: {repo.updated_at}</div>
			<a href={repo.html_url} className="repo__link">Ссылка на репозиторий</a>
		</div>
	)
}