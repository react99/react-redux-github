import './main.scss'
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getRepos} from "../api/api";
import {ReposItem} from "./ReposItem";
import {setCurrentPage} from "../reducers/repos";
import {createPages} from "../utils/pageCreator";

export const Main = () => {
	const dispatch = useDispatch();
	const repos = useSelector(state => state.repos.items);
	const isFetching = useSelector(state => state.repos.isFetching);
	const isFetchingErrors = useSelector(state => state.repos.isFetchingErrors);
	const currentPage = useSelector(state => state.repos.currentPage);
	const perPage = useSelector(state => state.repos.perPage);
	const totalCount = useSelector(state => state.repos.totalCount);
	const [searchValue, setSearchValue] = useState('');

	const pagesCount = Math.ceil(totalCount / perPage)
	const pages = createPages(currentPage, pagesCount);

	useEffect(() => {
		dispatch(getRepos(searchValue, perPage, currentPage))
	}, [currentPage]) // eslint-disable-line react-hooks/exhaustive-deps

	const searchHandler = () => {
		dispatch(setCurrentPage(1))
		dispatch(getRepos(searchValue, perPage, currentPage))
	}

	return (
		<div className="repos-list">
			{isFetchingErrors &&
			<div className="alert alert-danger" role="alert">
				A simple danger alert—check it out!
			</div>
			}
			<div className="repos-list__search">
				<input type="text"
					   value={searchValue}
					   onChange={(e) => setSearchValue(e.target.value)}
					   placeholder="Input repo name"
					   className="repos-list__search-input"
				/>
				<button onClick={searchHandler} className="repos-list__search-button">Search</button>
			</div>
			{
				isFetching === false ?
					repos.map(repo => {
						return <ReposItem key={repo.id} repo={repo}/>
					})
					:
					<div className="is-fetching"/>
			}
			<div className="pagination">
				{pages.map((page, idx) => {
					return <span
						key={idx}
						className={currentPage === page ? "pagination__current" : "pagination__page"}
						onClick={() => dispatch(setCurrentPage(page))}
					>{page}</span>
				})}
			</div>
		</div>
	)
}