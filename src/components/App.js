import './app.scss'
import React from "react";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";

import {Main} from "./Main";
import {Card} from "./Card";

function App() {
	return (
		<BrowserRouter>
			<div className="container">
				<Switch>
					<Route exact path="/" component={Main}/>
					<Route path="/card/:username/:reponame" component={Card}/>
					<Redirect to="/" />
				</Switch>
			</div>
		</BrowserRouter>
	);
}

export default App;
