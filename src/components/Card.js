import './card.scss';
import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {getCurrentRepo, getContributors} from "../api/api";
import {useDispatch, useSelector} from "react-redux";

export const Card = (props) => {
	const {username, reponame} = useParams()
	const [repo, setRepo] = useState({})
	const [contributors, setContributors] = useState([])
	const isFetching = useSelector(state => state.repos.isFetching);
	const dispatch = useDispatch();

	useEffect(() => {
		getCurrentRepo(username, reponame, setRepo, dispatch);
		getContributors(username, reponame, setContributors);
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<div>
			{isFetching === false ?
				<div>
					<button onClick={props.history.goBack} className="back-btn">Back</button>
					<div className="card-repo">
						<img src={repo.owner?.avatar_url} className="card-repo__img" alt=""/>
						<div className="card-repo__name">{repo.name}</div>
						<div className="card-repo__stars">{repo.stargazers_count}</div>
					</div>
					<div style={{margin: 10, fontSize: '20px', fontWeight: 'bold'}}>Contributers:</div>
					<ul>
						{contributors.map(({login}, idx) => {
							return <div key={idx}>{idx + 1}. {login}</div>
						})}
					</ul>
				</div>
				:
				<div className="is-fetching"/>
			}
		</div>
	)
}