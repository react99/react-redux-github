const SET_REPOS = 'SET_REPOS';
const SET_IS_FETCHING = 'SET_IS_FETCHING';
const SET_FETCHING_ERROR = 'SET_FETCHING_ERROR';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';

export const setRepos = payload => ({type: SET_REPOS, payload})
export const setIsFetching = payload => ({type: SET_IS_FETCHING, payload})
export const setFetchingError = payload => ({type: SET_FETCHING_ERROR, payload})
export const setCurrentPage = payload => ({type: SET_CURRENT_PAGE, payload})

const defaultState = {
	items: [],
	isFetching: true,
	isFetchingErrors: false,
	currentPage: 1,
	perPage: 10,
	totalCount: 0,
}

export const repos = (state = defaultState, action) => {
	switch (action.type) {
		case SET_REPOS :
			return {
				...state,
				items: action.payload.items,
				totalCount: action.payload.total_count,
				isFetching: false,
			}
		case SET_IS_FETCHING :
			return {
				...state,
				isFetching: action.payload,
			}
			case SET_CURRENT_PAGE :
			return {
				...state,
				currentPage: action.payload,
			}
			case SET_FETCHING_ERROR :
			return {
				...state,
				isFetchingErrors: action.payload,
			}
		default:
			return state;

	}
}

