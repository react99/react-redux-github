import axios from "axios";
import {setIsFetching, setRepos, setFetchingError} from "../reducers/repos";

export const getRepos = (searchQuery, perPage, currentPage) => dispatch => {

	searchQuery = searchQuery ? searchQuery : 'stars:>=1';

	const url = new URL('/search/repositories', 'https://api.github.com');
	url.searchParams.set('q', searchQuery);
	url.searchParams.set('sort', 'stars');
	url.searchParams.set('per_page', perPage);
	url.searchParams.set('page', currentPage);

	dispatch(setIsFetching(true))

	axios.get(url.toString())
		.then(response => {
			dispatch(setRepos(response.data))
		})
		.catch(() => {
			dispatch(setIsFetching(false))
			dispatch(setFetchingError(true))
			setTimeout(() => {
				dispatch(setFetchingError(false))
			}, 2000)
		})
}

export const getCurrentRepo = (owner, repo, setRepo, dispatch) => {
	const url = new URL(`/repos/${owner}/${repo}`, 'https://api.github.com');
	dispatch(setIsFetching(true))
	axios.get(url.toString())
		.then(response => {
			setRepo(response.data)
			dispatch(setIsFetching(false))
		})
}

export const getContributors = (owner, repo, setContributors) => {
	const url = new URL(`/repos/${owner}/${repo}/contributors`, 'https://api.github.com');
	url.searchParams.set('per_page', '10');
	url.searchParams.set('page', '1');

	axios.get(url.toString())
		.then(response => {
			setContributors(response.data)
		})
}