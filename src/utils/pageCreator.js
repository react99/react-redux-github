export const createPages = (currentPage, pagesCount) => {
	const arr = [];
	if (pagesCount <= 10) {
		for (let i = 1; i <= 10; i++) {
			arr.push(i)
		}
	}

	if (pagesCount > 10) {
		if (currentPage > 5) {
			for (let i = currentPage - 4; i <= currentPage + 5; i++) {
				arr.push(i)
				if (i === pagesCount) break;
			}
		} else {
			for (let i = 1; i <= 10; i++) {
				arr.push(i)
				if (i === pagesCount) break;
			}
		}
	}

	return arr;
}